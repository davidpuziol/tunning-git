#!/bin/bash
echo "Configurando usuário"
git config --global user.name "David Puziol Prata"
git config --global user.email "davidpuziol@gmail.com"

echo "Configurando editor para o VSCode"
git config --global core.editor "code"

echo "Configurando Colors"
git config --global color.branch "auto"
git config --global color.diff "auto"
git config --global color.interactive "auto"
git config --global color.status "auto"

echo "Criando alias"
git config --global alias.ec "git config --global -e"
git config --global alias.co "checkout"
git config --global alias.cob "checkout -b"
git config --global alias.c "commit"
git config --global alias.cm "commit -m"
git config --global alias.cma "commit -a --amend"
git config --global alias.lg "log — all — graph — decorate — oneline — abbrev-commit"
git config --global alias.ac '!git add -A && git commit -m'
git config --global alias.df "diff"
git config --global alias.a  "add"
git config --global alias.aa "add -A"
git config --global alias.tags "tag -l"
git config --global alias.branches "branches -a"
git config --global alias.up '!git pull --rebase --prune $@ && git submodule update --init --recursive'

git config --global alias.save '!git add -A && git commit -m "SAVEPOINT"'
git config --global alias.undo "reset HEAD~1 --mixed"
git config --global alias.wipe '!git add -A && git commit -qm "WIPE SAVEPOINT" && git reset HEAD~1 --hard'
git config --global alias.pullall '!git branch -r | grep -v "\->" | while read remote; do git branch --track ${remote#origin/} ${remote}; done && git fetch --all && git pull --all'
